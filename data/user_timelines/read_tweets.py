import sys
import json
import pandas as pd 

tweets_file = sys.argv[1]
before = sys.argv[2]
tweets_file = open(tweets_file, "r") 
tweets = tweets_file.readlines()
## there should only be one line in the file 
tweet = json.loads(tweets[0])
created_at = []
tweet_id = []
text = []
author_id = []
df = pd.DataFrame() 
def get_data():
	if 'data' not in tweet.keys():
		print("0") 
		sys.exit(0)
	if tweet['data']:
		## there's some data in here so let's parse it into a dataframe 
		data = tweet['data']
		for result in data:
			created_at.append(result['created_at']) 
			tweet_id.append(result['id']) 
			text.append(result['text'])
			author_id.append(result['author_id']) 
		df['Created At'] = created_at
		df['Tweet ID'] = tweet_id
		df['Text'] = text
		df['Author ID'] = author_id
		df['Before Harassment'] = [before] * len(text) 
		df.to_csv('user_tweet_data.csv', mode='a', index=False, header=False)
	if tweet['meta']:
		if 'next_token' in tweet['meta'].keys():
			print(tweet['meta']['newest_id'])
			sys.exit(3)
			#sys.exit(tweet['meta']['newest_id'])
		else:
			print("0") 
			sys.exit(0)
	else:
		print("0") 
		sys.exit(0)

(get_data())
