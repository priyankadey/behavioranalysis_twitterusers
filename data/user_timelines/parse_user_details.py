import json
import pandas as pd

data = open("./user_details.txt") 
harassment_info = input("Enter the harassment file again (need for the dates): ") 
har = open(harassment_info) 
har_lines = har.readlines()
lines = data.readlines()

df = pd.DataFrame()
usernames = []
tids = []
harassment_dates = []

for i, line in enumerate(lines):
	line = (line[:-1])
	d = json.loads(line)
	try: 
		username = d['data'][0]['username']  
		tid = (d['data'][0]['id'])
		har_date = har_lines[i].split(",")[1][:-1] 
		print(har_date)
		usernames.append(username)
		tids.append(tid)
		harassment_dates.append(har_date)
	except:
		pass

df['u'] = usernames
df['id'] = tids
df['hd'] = harassment_dates
df.to_csv("timeline_query_params.csv", index = False, header = False) 
