import json
import re
import csv

dedup_dict = {}

with open('2020_data_unverified_users.csv', 'r') as csvfile1:
    print("Entered 2020")
    tweetsreader = csv.reader(csvfile1, delimiter=',')
    for row in tweetsreader:
        user = row[0].lower()
        if user in dedup_dict.keys():
            print("Duplicate!", user)
        else:
            dedup_dict[user] = row[1]

#print(dedup_dict)

with open('2021_data_unverified_users.csv', 'r') as csvfile2:
    print("Entered 2021")
    tweetsreader = csv.reader(csvfile2, delimiter=',')
    for row in tweetsreader:
        user = row[0].lower()
        if user in dedup_dict.keys():
            print("Duplicate!", user)
        else:
            dedup_dict[user] = row[1]

with open('2021_data_unverified_users_2.csv', 'r') as csvfile2:
    print("Entered 2021 2")
    tweetsreader = csv.reader(csvfile2, delimiter=',')
    for row in tweetsreader:
        user = row[0].lower()
        if user in dedup_dict.keys():
            print("Duplicate!", user)
        else:
            dedup_dict[user] = row[1]

#print(dedup_dict)

with open('datasets_2015-2017_ppdata.csv', 'r') as csvfile3:
    print("Entered 2015-17")
    tweetsreader = csv.reader(csvfile3, delimiter=',')
    for row in tweetsreader:
        user = row[0].lower()
        if user[0] == '@':
            user = user[1:]
        if user in dedup_dict.keys():
            print("Duplicate!", user)
        else:
            dedup_dict[user] = row[1]

#print(dedup_dict)