UPDATE: You can run get_tweets.sh, if above does not work, then follow steps below:

1. It is expected that there is a csv file with username, harassment date (the date of the tweet we looked at) 
2. Rm user_details.txt and then Run ./get_user_info.sh 
3. Run python3 parse_user_details.py, this will generate the final query_timeline file needed to get tweets before & after harassment 
4. Run ./get_user_tweets_before_harassment.sh, this will append to user_tweet_data.csv (or create if not exist) 
5. Run ./get_user_tweets_after_harassment.sh, this will append to user_tweet_data.csv
