import json
import re
import csv

#tweet_csv_files = ['bimbo_2020_filtered.tsv', 'bitch_2020_filtered.tsv', 'fuckingbitch_2020_filtered.tsv', 'high_filter_1_filtered.tsv', 'high_filter_2_filtered.tsv', 'skank_2020_filtered.tsv', 'slutbag_2020_filtered.tsv', 'whore_2020_filtered.tsv']
tweet_csv_files = ['../2021_2_filtered.tsv']

users_dict = {}

for csv_file in tweet_csv_files:
    with open(csv_file, 'r') as csvfile:
        tweetsreader = csv.reader(csvfile, delimiter='\t')
        next(tweetsreader)
        for row in tweetsreader:
            print(row[2])
            user = re.findall('@[^ ]*', row[2])[0][1:].lower()
            timestamp = row[1]
            if user in users_dict.keys():
                continue
            else:
                users_dict[user] = {'timestamp': timestamp, 'tweet': row[2], 'tweet_id': row[0], 'user_id': row[8]}

print(len(users_dict))

csv_output_file = '../2021_data_unverified_users_2.csv'
csv_output_rows = []

for key, value in users_dict.items():
    csv_output_rows.append([key, value['timestamp']])

with open(csv_output_file, 'w') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',')
    csvwriter.writerows(csv_output_rows)


#full_output_file = '2020_data_complete.csv'
full_output_file = '../2021_data_complete_2.csv'
full_output_rows = []

for key, value in users_dict.items():
    full_output_rows.append([key, value['timestamp'], value['tweet'].replace(',', ''), value['tweet_id'], value['user_id']])

fields = ['username', 'timestamp', 'tweet', 'tweet_id', 'user_id']

with open(full_output_file, 'w') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',')
    csvwriter.writerow(fields)
    csvwriter.writerows(full_output_rows)
