import json
import re
import csv

dedup_dict = {}

with open('2020_data_unverified_users.csv', 'r') as csvfile:
    tweetsreader = csv.reader(csvfile, delimiter=',')
    for row in tweetsreader:
        user = row[0].lower()
        if user in dedup_dict.keys():
            print("Duplicate!")
        else:
            dedup_dict[user] = row[1]