import json
import re
import csv

#tweet_tsv_file = 'slutbag_2020.tsv'
#tweet_tsv_file_fl = 'slutbag_2020_filtered.tsv'

tweet_tsv_file = '../2021_2.tsv'
tweet_tsv_file_fl = '../2021_2_filtered.tsv'

final_rows = []
fields = []

with open(tweet_tsv_file, 'r') as csvfile:
    tweetsreader = csv.reader(csvfile, delimiter='\t')
    flag = 0
    for row in tweetsreader:
        if flag == 0:
            fields = row
            flag = 1
        else:
            if row[5] != 'Y':
                continue
            final_rows.append(row)


with open(tweet_tsv_file_fl, 'w') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter='\t')
    csvwriter.writerow(fields)
    # writing the data rows
    csvwriter.writerows(final_rows)
