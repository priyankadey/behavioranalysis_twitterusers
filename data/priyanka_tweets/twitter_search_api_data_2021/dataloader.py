import pandas as pd
import json 
from os import listdir
from os.path import isfile, join

files = [f for f in listdir("./") if isfile(join("./", f))]
sources, tweet_ids, author_ids, texts, created_at, retweet_count, reply_count, like_count, quote_count = [], [], [], [], [], [], [], [], []
for f in files:
	if(f[-4:]) == "json":
		print(f)
		data = open(f)
		data_lines = data.readlines()
		for line in data_lines:
			line = json.loads(line)
			cur_data = line['data']
			for tweet in cur_data:
				sources.append(tweet['source'])
				tweet_ids.append(tweet['id']) 
				author_ids.append(tweet['author_id']) 
				texts.append(tweet['text']) 
				created_at.append(tweet['created_at']) 
				public_metrics = tweet['public_metrics']
				retweet_count.append(public_metrics['retweet_count']) 
				reply_count.append(public_metrics['reply_count']) 
				like_count.append(public_metrics['like_count']) 
				quote_count.append(public_metrics['quote_count']) 

df = pd.DataFrame()
df['source'] = sources
df['tweet_ids'] = tweet_ids
df['author_ids'] = author_ids
df['texts'] = texts
df['created_at'] = created_at
df['retweet_count'] = retweet_count 
df['reply_count'] = reply_count 
df['like_count'] = like_count
df['quote_count'] = quote_count 
print(df.head())
print(len(df))
df.to_csv("2021_unfiltered_jantoapril_data.csv") 
