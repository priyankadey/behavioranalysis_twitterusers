import pandas as pd
import json
from pandas import json_normalize

user_details = open("./user_details.txt") 
users = user_details.readlines()
keys = ['username', 'name', 'description', 'pinned_tweet_id', 'listed_count', 'following_count', 'created_at', 'id', 'tweet_count', 'location', 'followers_count'] 
df = pd.DataFrame(columns=keys)
count = 0
for line in users:
	print(len(users))
	count += 1
	data = {}
	user = json.loads(line)
	user = (user['data'][0])
	for key in user:
		if key != 'public_metrics':
			data[key] = user[key]
	print(user['public_metrics'])
	metrics = user['public_metrics'] 
	print(metrics)
	for metric in metrics.keys():
		data[metric] = metrics[metric]
	df = df.append(data, ignore_index = True)
harassment_data = input("Enter the file which contains the harassment data (assuming this data is a csv): ") 
data = pd.read_csv(harassment_data) 
harassment_dates = list(data['created_at'])
df['harassment_dates'] = harassment_dates
user_details.close()
df = df.fillna("N/A") 
df.to_csv("user_details.parsed.csv", index=False)
ids = list(df['id'].values)
dates = list(df['harassment_dates']) 
filee = open("timeline_query_params.csv", "w") 
for i, idd in enumerate(ids):
	s = idd + "," + dates[i] + "\n"
	filee.write(s) 
filee.close()
	
