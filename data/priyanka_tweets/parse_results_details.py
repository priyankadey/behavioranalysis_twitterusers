import json
import pandas as pd

results_file = open("./results_details.txt") 
orig_results_file = open("./results.txt") 
orig = orig_results_file.readlines()
lines = results_file.readlines()
created = []
users = []
for i, line in enumerate(lines):
	user = orig[i].split(",")[0] 
	line = line[:-1]
	d = json.loads(line)
	try: 
		created_at = d['data']['created_at']		
		created.append(created_at) 
		users.append(user) 
	except:
		pass

df = pd.DataFrame()
df['u'] = users
df['c'] = created
df.to_csv("dataset&2015-2017_preprocessed_data.csv", header=False, index=False) 
