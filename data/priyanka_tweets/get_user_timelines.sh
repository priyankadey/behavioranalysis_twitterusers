#!/bin/bash

while IFS="" read -r p || [ -n "$p" ]
do
  curl --location --request GET 'https://api.twitter.com/2/users/by?usernames='${p}'&user.fields=created_at,location,description,public_metrics,withheld&expansions=pinned_tweet_id&tweet.fields=author_id,created_at' \
--header 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAHITVAEAAAAABYI2C8C8t4NDHFhmhi%2Fqluj4QzU%3DiXGZHK53Z9TsmxnCGeJK0dwzMdyheiPs8uKyMNefrWb5A3oTVo' \
--header 'Cookie: guest_id=v1%3A163599093220626365; personalization_id="v1_Xd5fFqPbvibgVpTsvTih1A=="' >> user_details.txt

  echo "" >> user_details.txt 
done < users.txt

## we need to parse the data & add the harassment information 
python3 parse.py

## need to read the file and get the tweets before and after the harassment stage 
while IFS=, read -r id date
do
##paginate through the results 
    echo "$id and $date"
    echo "$id" >> tweets_before.txt
    curl --location --request GET 'https://api.twitter.com/2/users/'${id}'/tweets?tweet.fields=created_at&expansions=author_id&user.fields=created_at&start_time=2010-11-06T00:00:00.000Z&end_time='${date} \
--header 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAHITVAEAAAAABYI2C8C8t4NDHFhmhi%2Fqluj4QzU%3DiXGZHK53Z9TsmxnCGeJK0dwzMdyheiPs8uKyMNefrWb5A3oTVo' \
--header 'Cookie: guest_id=v1%3A163599093220626365; personalization_id="v1_Xd5fFqPbvibgVpTsvTih1A=="' >> tweets_before.txt
    echo "" >> tweets_before.txt
    echo "$id" >> tweets_after.txt
    curl --location --request GET 'https://api.twitter.com/2/users/'${id}'/tweets?tweet.fields=created_at&expansions=author_id&user.fields=created_at&start_time='${date}'&end_time=2021-11-06T00:00:00.000Z' \
--header 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAAHITVAEAAAAABYI2C8C8t4NDHFhmhi%2Fqluj4QzU%3DiXGZHK53Z9TsmxnCGeJK0dwzMdyheiPs8uKyMNefrWb5A3oTVo' \
--header 'Cookie: guest_id=v1%3A163599093220626365; personalization_id="v1_Xd5fFqPbvibgVpTsvTih1A=="' >> tweets_after.txt
    echo "" >> tweets_after.txt
# 1173857154777923584
done < timeline_query_params.csv
